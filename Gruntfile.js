'use strict';
var opt = require('./tools/build');
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            dist: [
                '<%= pkg.deployFolder %>/',
            ]
        },
        copy: {
            dist: {
                files: [
                    {expand: true, cwd: '<%= pkg.devFolder %>/', src: ['**'], dest: '<%= pkg.deployFolder %>/'}
                ]
            }
        },
        requirejs: {
            compile: {
                options: opt
            }
        },
        recess: {
            dist: {
                options: {
                    compile: true,
                    compress: true
                },
                files: {
                    '<%= pkg.devFolder %>/assets/css/main.css': [
                        '<%= pkg.devFolder %>/assets/less/main.less'
                    ]
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                '<%= pkg.devFolder %>/assets/js/*.js',
                '<%= pkg.devFolder %>/assets/js/mylibs/*.js',
                '<%= pkg.devFolder %>/assets/js/mylibs/*/**.js'
            ]
        },
        watch: {
            less: {
                files: [
                    '<%= pkg.devFolder %>/assets/less/*.less',
                    '<%= pkg.devFolder %>/assets/less/*/**.less'
                ],
                tasks: ['recess', 'autoprefixer']
            },
            js: {
                files: [
                    '<%= jshint.all %>'
                ],
                tasks: ['jshint']
            },
            all: {
                options: { livereload: true },
                files: ['<%= pkg.devFolder %>/assets/css/main.css']
            }
        },
        autoprefixer: {
            options: {
              browsers: ['last 3 versions', 'ie 8']
            },
            dist: {
                files: {
                    '<%= pkg.deployFolder %>/assets/css/main.css': [
                        '<%= pkg.deployFolder %>/assets/css/main.css'
                    ]
                }
            }
        },
        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 5,
                    pngquant: false  // PNG Quant fails on my installation. Try this, it might work for you!
                },
                files: [{
                    expand: true,
                    cwd: '<%= pkg.deployFolder %>/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '<%= pkg.deployFolder %>/'
                }]
            }
        },
        "string-replace": {
            inline: {
                options: {
                    replacements: [
                        {
                            pattern: '<script src="assets/js/lib/modernizr-2.6.2.js"></script>',
                            replacement: '<script src="assets/js/lib/modernizr-custom.js"></script>'
                        },
                        {
                            pattern: '<script src="http://127.0.0.1:35729/livereload.js"></script>',
                            replacement: ''
                        },
                    ]
                },
                files: {
                    "<%= pkg.deployFolder %>/index.html" : "<%= pkg.deployFolder %>/index.html"
                }
            }
        },
        replace: {
            dist: {
                options: {
                    variables: {
                        'bust': '<%= pkg.version %>'
                    }
                },
                files: [
                    {src: ['<%= pkg.deployFolder %>/index.html'], dest: '<%= pkg.deployFolder %>/index.html'},
                    {src: ['<%= pkg.deployFolder %>/assets/js/common.js'], dest: '<%= pkg.deployFolder %>/assets/js/common.js'}
                ]
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['-a'], // '-a' for all files
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin'
            }
        },
        'ftp-deploy': {
            build: {
            auth: {
                host: 'kentlyonsdev2.com',
                port: 21,
                authKey: 'key1'
            },
            src: '<%= pkg.deployFolder %>',
            dest: 'kentlyonsdev2.com/test_deploy/',
            exclusions: [ '<%= pkg.deployFolder %>/**/.DS_Store',
                          '<%= pkg.deployFolder %>/**/Thumbs.db',
                          '<%= pkg.deployFolder %>/build.txt',
                          'dist/tmp' ]
            }
        },
        modernizr: {
            "devFile" : "<%= pkg.deployFolder %>/assets/js/lib/modernizr-2.6.2.js",
            "outputFile" : "<%= pkg.deployFolder %>/assets/js/lib/modernizr-custom.js",
            "extra" : {
                "shiv" : true,
                "printshiv" : false,
                "load" : true,
                "mq" : false,
                "cssclasses" : true,
                "uglify" : false
            },
            files: ['<%= pkg.deployFolder %>/assets/js/**/*.js',
                    '<%= pkg.deployFolder %>/assets/css/**/*.css',
                    '!<%= pkg.deployFolder %>/assets/js/lib/respond.js']
        }
    });

    // Load tasks from package automatically using matchdep
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Register tasks
    grunt.registerTask('build', [
        'clean',
        'copy',
        'requirejs',
        'replace',
        'imagemin',
        'modernizr',
        'string-replace'
    ]);

    grunt.registerTask('dev', [
        'watch'
    ]);

    grunt.registerTask('deploy', [
        'ftp-deploy'
    ]);

};