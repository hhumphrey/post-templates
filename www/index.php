<? include 'templates/head.php'; ?>

<body>

	<div role="main">

		<? include 'templates/header.php'; ?>

		<div class="two-col">
			<div class="two-col-main">
				<p>
					POST — is an independent London based design agency founded in 2013. We work with clients both large and small and collaborate with a talented network to produce designs for identity, branding, print, publishing, signage, advertising and websites.
				</p>
			</div>
		</div>

		<ul class="figure-list">
			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/1120x736" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 1</h3>
					</figcaption>
				</a>
			</li>
			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/544x802" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 2</h3>
					</figcaption>
				</a>
			</li>
			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/544x384" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 3</h3>
					</figcaption>
				</a>
			</li>
			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 4</h3>
					</figcaption>
				</a>
			</li>
			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/256x384" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 4</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/832x544" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 5</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/1120x736" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 6</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/256x384" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 7</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 8</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 9</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/544x800" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 10</h3>
					</figcaption>
				</a>
			</li>

			<li class="fl-item">
				<a href="/">
					<figure class="fl-figure">
						<img src="http://placehold.it/544x384" />
					</figure>
					<figcaption class="fl-caption">
						<h3>Project title 11</h3>
					</figcaption>
				</a>
			</li>

		</ul>

		<? include 'templates/footer.php'; ?>

	</div>

	<? include 'templates/scripts.php'; ?>

</body>
</html>