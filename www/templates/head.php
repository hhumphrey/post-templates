<!DOCTYPE>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<link rel="stylesheet" href="assets/css/main.css?bust=@@bust">

	<!-- Custom Modernizr is generated via grunt on build -->
	<script src="assets/js/lib/modernizr-2.6.2.js"></script>

	<!-- Add media query support for IE8 -->
	<!--[if (gte IE 6)&(lte IE 8)]>
		<script type="text/javascript" src="js/lib/respond.js"></script>
	<![endif]-->
</head>