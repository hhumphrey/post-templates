<? include 'templates/head.php'; ?>

<body>

	<div role="main">

		<? include 'templates/header.php'; ?>

		<section>
			<header>
				<h1>Title</h1>
			</header>
			<p>
				Project description monocle ipsum dolor sit amet conversation ryokan joy Wink creative global izakaya signature intricate exclusive. Airbus A380 sleepy Tsutaya iconic. Artisanal extraordinary Helsinki Comme des Garçons.
			</p>
		</section>

		<ul>
			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/1120x736" />
					</figure>
					<figcaption>
						<h3>Project title 1</h3>
					</figcaption>
				</a>
			</li>
			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/544x802" />
					</figure>
					<figcaption>
						<h3>Project title 2</h3>
					</figcaption>
				</a>
			</li>
			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/544x384" />
					</figure>
					<figcaption>
						<h3>Project title 3</h3>
					</figcaption>
				</a>
			</li>
			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption>
						<h3>Project title 4</h3>
					</figcaption>
				</a>
			</li>
			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/256x384" />
					</figure>
					<figcaption>
						<h3>Project title 4</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/832x544" />
					</figure>
					<figcaption>
						<h3>Project title 5</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/1120x736" />
					</figure>
					<figcaption>
						<h3>Project title 6</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/256x384" />
					</figure>
					<figcaption>
						<h3>Project title 7</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption>
						<h3>Project title 8</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/256x192" />
					</figure>
					<figcaption>
						<h3>Project title 9</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/544x800" />
					</figure>
					<figcaption>
						<h3>Project title 10</h3>
					</figcaption>
				</a>
			</li>

			<li>
				<a href="/">
					<figure>
						<img src="http://placehold.it/544x384" />
					</figure>
					<figcaption>
						<h3>Project title 11</h3>
					</figcaption>
				</a>
			</li>

		</ul>

		<? include 'templates/footer.php'; ?>

	</div>

	<? include 'templates/scripts.php'; ?>

</body>
</html>