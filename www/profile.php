<? include 'templates/head.php'; ?>

<body>

	<div role="main">

		<? include 'templates/header.php'; ?>

		<article>
			<section>
				<h1>Title</h1>
				<p>Monocle ipsum dolor sit amet hub airport Helsinki cosy bulletin, global Comme des Garçons exclusive soft power. Espresso first-class cutting-edge, exclusive carefully curated alluring destination iconic punctual. Beams Sunspel eclectic Washlet, wardrobe St Moritz Ettinger smart artisanal Boeing 787 Baggu exclusive premium. Baggu liveable Singapore Washlet uniforms perfect smart vibrant iconic flat white Airbus A380 extraordinary Muji.</p>
			</section>
			<section>
				<h1>Title</h1>
				<p>Monocle ipsum dolor sit amet hub airport Helsinki cosy bulletin, global Comme des Garçons exclusive soft power. Espresso first-class cutting-edge.</p> 
				<p>Exclusive carefully curated alluring destination iconic punctual. Beams Sunspel eclectic Washlet, wardrobe St Moritz Ettinger smart artisanal Boeing 787 Baggu exclusive premium. Baggu liveable Singapore Washlet uniforms perfect smart vibrant iconic flat white Airbus A380 extraordinary Muji.</p>
			</section>
			<section>
				<h1>Title</h1>
				<ul>
					<li>
						<figure>
							<img src="http://placehold.it/256x384" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>

					<li>
						<figure>
							<img src="http://placehold.it/256x384" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>

					<li>
						<figure>
							<img src="http://placehold.it/256x384" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>
				</ul>	
			</section>

			<section>
				<h1>Title</h1>
				<ul>
					<li>
						<figure>
							<img src="http://placehold.it/832x544" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>
					<li>
						<figure>
							<img src="http://placehold.it/832x544" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>
					<li>
						<figure>
							<img src="http://placehold.it/832x544" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>
				</ul>
			</section>
		</article>

		<? include 'templates/footer.php'; ?>

	</div>

	<? include 'templates/scripts.php'; ?>

</body>
</html>