define([
    'jquery',
    'jqueryMobileTouch',
    'modules/global',
    'modules/example',
    'jquerySmartResize'
],

function ( $, jqueryMobileTouch, Global, Example, jquerySmartResize ) {

    console.log("home bootstrapper loaded");

    Global.init();
    Example.init();

    $(window).smartresize(function(){
		console.log( Global.detectWidth() );
	});
});