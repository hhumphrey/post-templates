<? include 'templates/head.php'; ?>

<body>

	<div role="main">

		<? include 'templates/header.php'; ?>

		<article>
			<section>
				<header>
					<h1>Title</h1>
				</header>
				<p><a href="mailto:mailroom@deliverybypost.com">mailto:mailroom@deliverybypost.com</a></p>
			</section>
			<section>
				<header>
					<h1>Title</h1>
				</header>
				<p>+44 775643380</p>
			</section>
			
			<section>
				<header>
					<h1>Title</h1>
				</header>
				<p>
					Studio 10<br />
					Ovanna Mews<br />
					19A Buckingham Road<br />
					London N1 4EY
				</p>
			</section>

			<section>
				<header>
					<h1>Title</h1>
				</header>
				<ul>
					<li>
						<figure>
							<img src="http://placehold.it/832x544" />
						</figure>
						<figcaption>
							<h3>Project title 1</h3>
						</figcaption>
					</li>
				</ul>
			</section>

			<section>
				<header>
					<h1>Title</h1>
				</header>
				<p>
					Twitter<br />
					Instagram<br />
					Newsletter
				</p>
			</section>
		</article>

		<? include 'templates/footer.php'; ?>

	</div>

	<? include 'templates/scripts.php'; ?>

</body>
</html>