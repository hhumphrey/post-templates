module.exports = {
    appDir: 'www/assets',
    mainConfigFile: 'www/assets/js/common.js',
    dir: 'www-deploy/assets',
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'common',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: [
              'bootstrapCarousel'
            ],
            // Since we bundle jquery with require.js we don't need
            // to additionally compile it into this layer. It will
            // already be available on the page as part of the 
            // script tag that pulls in require.js
            exclude: ['jquery']
        },

        //Now set up a build layer for each main layer, but exclude
        //the common one. "exclude" will exclude nested
        //the nested, built dependencies from "common". Any
        //"exclude" that includes built modules should be
        //listed before the build layer that wants to exclude it.
        //The "page1" and "page2" modules are **not** the targets of
        //the optimization, because shim config is in play, and
        //shimmed dependencies need to maintain their load order.
        //In this example, common.js will hold jquery, so backbone
        //needs to be delayed from loading until common.js finishes.
        //That loading sequence is controlled in page1.js.
        {
            //module names are relative to baseUrl/paths config
            name: 'main-page1',
            exclude: ['common', 'jquery']
        }

    ] 
}