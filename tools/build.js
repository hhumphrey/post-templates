module.exports = {
    appDir: 'www',
    mainConfigFile: 'www/assets/js/common.js',
    dir: 'www-deploy',
    baseUrl: 'assets/js',
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'common',
            include: ['jquery', 'jqueryMobileTouch']
        },

        /*
            Now set up a build layer for each main layer, but exclude
            the common one. "exclude" will exclude nested
            the nested, built dependencies from "common". Any
            "exclude" that includes built modules should be
            listed before the build layer that wants to exclude it.
        */
        {
            //module names are relative to baseUrl/paths config
            name: 'home',
            exclude: [  'jquery', 'jqueryMobileTouch' ]
        }
    ]
}
